<?php

namespace Hoop\Hydro\PageTypes;

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HeaderField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Security\InheritedPermissions;
use SilverStripe\Security\InheritedPermissionsExtension;
use SilverStripe\Security\Member;
use SilverStripe\Security\Permission;
use SilverStripe\Security\PermissionChecker;
use SilverStripe\Security\PermissionProvider;
use SilverStripe\Versioned\Versioned;

class FeaturedItem extends DataObject {
	
	private static $db = [
		'FeaturedTitle' => 'Varchar(64)',
		'FeaturedText' => 'Varchar(160)',
		'FeaturedInfo' => 'Varchar(48)',
		'FeaturedClass' => 'Varchar(64)',
		'Icon' => 'Varchar(192)',
		'ButtonText' => 'Varchar(48)',
		'ExternalLink' => 'Varchar(255)',
		'OnlyImage' => 'Boolean',
		'Published' => 'Boolean'
	]; 
	
	private static $has_one = [
		'FeaturedImage' => Image::class,
		'LinkLoc' => SiteTree::class,
		'Attachment' => File::class
	];

	private static $belongs_many_many = [
		'Pages' => SiteTree::class
	];
	
	private static $summary_fields = [
		'FeaturedTitle',
		'FeaturedImage.CMSThumbnail'
	];

	private static $searchable_fields = [
		'FeaturedTitle'
	];
	
	/* !FieldLabels Translation */

	function fieldLabels($includerelations = true) {
		return [
			'FeaturedImage.CMSThumbnail' => _t(__CLASS__ . '.IMAGE', 'Image'),
			'FeaturedTitle' => _t(__CLASS__ . '.HEADING', 'Heading'),
			'Published' => _t(__CLASS__ . '.PUBLISHED', 'Published')
		];
	}
	
	/* !Set Default values */
	private static $defaults = [

		'Published' => true,
		'OnlyImage' => false

	];
	
	function canView($member = null) {
		return true;
	}
	
	function canCreate($member = null) {
		return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
	}
	
	function canEdit($member = null) {
		return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
	}
	
	function canDelete($member = null) {
		return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
	}
	
	function getTitle() {
		return $this->FeaturedTitle;
	}
	
	function getCMSFields() {

		$f = new FieldList();

		// Include link switcher JavaScript
		// Requirements::javascript('js-library/linkSwitcher.js');
		
		// Settings for UploadField : Featured Image
		$UploadField = UploadField::create('FeaturedImage', _t(__CLASS__ . '.FEATUREDIMAGE','Featured Image'))
			->setAllowedFileCategories('image')
			->setFolderName('Uploads/images/featured');
		
		// The three options for which type of link to add
		$linkOptions = [
			"LinkLocID"=> _t(__CLASS__ . '.INTERNALLINK','Link to an internal page'),
			"ExternalLink" => _t(__CLASS__ . '.EXTERNALLINK','Link to an external website'),
			"Attachment" => _t(__CLASS__ . '.ATTACHMENT','Link to an attachment')
		];
		
		// If we've set an internal link already, then that option should be pre-selected
		$selectedOption = ($this->LinkLocID) 
										? 'LinkLocID' 
										:	(($this->AttachmentID) 
															? 'Attachment'
															: 'ExternalLink'
											);
		$linkTypeField = OptionsetField::create("LinkType", "", $linkOptions, $selectedOption);

		/* Create DropDown */
		// DropDown to select link from SiteTree

		$treedropdownfield = TreeDropdownField::create('LinkLocID', _t(__CLASS__ . '.INTERNALLINK','Internal link'), 'SiteTree')
			->addExtraClass('switchable');

		$externalURLField = TextField::create("ExternalLink", _t(__CLASS__ . '.EXTERNALLINK','External website url instead of internal link'))
			->setRightTitle (_t(__CLASS__ . '.EXTERNALLINKHELP','Remember to add full URL with http://'))
			->addExtraClass('switchable');

		// Settings for UploadField : Attachment
		$uploadField2 = UploadField::create('Attachment', _t(__CLASS__ . '.ATTACHMENT','Linked Attachment'))
			->addExtraClass('switchable')
			->setAllowedFileCategories('image', 'doc')
			->setFolderName('Uploads/attachments')
			->setDescription (_t(__CLASS__ . '.ATTACHEMNTURLHELP','Attachments are saved in attachments-folder'));
		
		/* !Create Admin Tabs */

		$t = TabSet::create(
			'Root',
			Tab::create(
				'Main',
				CheckboxField::create('OnlyImage', _t(__CLASS__ . '.SHOWONLYIMAGE','Show only image and hide all texts?')),
				TextField::create("FeaturedTitle", _t(__CLASS__ . '.FEATUREDTITLE','Featured title'))
					->displayUnless("OnlyImage")->isChecked()->end(),
				TextareaField::create("FeaturedText", _t(__CLASS__ . '.FEATUREDTEXT','Featured Text'))
					->displayUnless("OnlyImage")->isChecked()->end(),				
				$linkTypeField, $treedropdownfield, $externalURLField, $uploadField2,
				TextField::create("FeaturedInfo", _t(__CLASS__ . '.FEATUREDINFO','Additional info (price, new, hot, special offer, etc. or read more)'))
					->displayUnless("OnlyImage")->isChecked()->end(),
				TextField::create("FeaturedInfoClass", _t(__CLASS__ . '.FEATUREDINFOCLASS','CSS-class for styling'))
					->displayUnless("OnlyImage")->isChecked()->end()
					->setDescription (_t(__CLASS__ . '.FEATUREDINFOCLASSHELP','You can create e.g. ribbons (ribbon-primary)')),
				TextField::create("ButtonText", _t(__CLASS__ . '.BUTTONTEXT','Button text'))
					->displayUnless("OnlyImage")->isChecked()->end()

			),

			Tab::create(
				'Image',
				_t(__CLASS__ . '.TABIMAGE', 'Image'),
				$UploadField,
				TextField::create('FeaturedImageAlt', _t(__CLASS__ . '.ALTTEXT','Alt-text'))
					->setDescription (_t(__CLASS__ . '.ALTTEXTHELP','Alt-description of the image'))
					->displayUnless('OnlyImage')->isChecked()->end(),
				TextField::create('FontAwesomeIcon', _t(__CLASS__ . '.FEATUREDFAICON','FA-icon'))
					->displayUnless('OnlyImage')->isChecked()->end()
					->setDescription (_t(__CLASS__ . '.FEATUREDFAICONHELP','You can use icons from Font-Awesome (http://fontawesome.io). Usage: fa fa-camera-retro fa-5x'))
			),

			Tab::create(
				'Publishing',
				_t(__CLASS__ . '.TABPUBLISHING', 'Publishing'),
				CheckboxField::create('Published', _t(__CLASS__ . '.PUBLISHED','Published?'))
			)
		);
		
		$f->push($t);
		
		return $f;
	}

	/**
	 * @return void
	 */
	public function onBeforeWrite() {
		// If we've set an external link unset any existing internal link
		if($this->AttachmentID && $this->isChanged('AttachmentID')) {
			$this->LinkLocID = false;
			$this->ExternalLink = false;
		// Otherwise, if we've set an internal link unset any existing external link
		} elseif($this->ExternalLink && $this->isChanged('ExternalLink')) {
			$this->LinkLocID = false;
			$this->AttachmentID = false;
		} elseif($this->LinkLocID && $this->isChanged('LinkLocID')) {
			$this->ExternalLink = false;
			$this->AttachmentID = false;
		} 	
		parent::onBeforeWrite();
	}

	/**
	 * Fetch the current link, use with $Link in templates
	 * @return string|false
	 */
	public function getLink() {
		if($this->Attachment() && $this->Attachment()->exists()) {
			return $this->Attachment()->Link();
		} elseif($this->ExternalLink) {
			return $this->ExternalLink;
		} elseif($this->LinkLoc() && $this->LinkLoc()->exists()) {
			return $this->LinkLoc()->Link();
		}
		return false;
	}
}
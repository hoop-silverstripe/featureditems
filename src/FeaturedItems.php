<?php

namespace Hoop\Hydro\PageTypes;

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HeaderField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldButtonRow;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;
use SilverStripe\Forms\GridField\GridFieldToolbarHeader;
use SilverStripe\ORM\DataExtension;

class FeaturedItems extends DataExtension {

	private static $singular_name = 'Featured';
	private static $plural_name = 'FeaturedItems';
	private static $description = 'Featured Items';
	private static $icon = '';

	private static $db = [
		'FeaturedElementTitle' => 'Varchar(64)',
		'FeaturedElementSubtitle' => 'Varchar(96)',
		'FeaturedElementContent' => 'HTMLText'
	]; 

	private static $many_many = [
		"FeaturedItems" => "FeaturedItem"
	];
	
	private static $many_many_extraFields = [
		'FeaturedItems' => [
			'SortOrder' => 'Int'
		]
	];
	
	function updateCMSFields(FieldList $fields) {

		$fields->findOrMakeTab('Root.FeaturedItems', _t(__CLASS__ . '.TABFEATURED','Featured Items'));
		$fields->addFieldsToTab('Root.FeaturedItems', [
			HeaderField::create(_t(__CLASS__ . '.FEATUREDELEMENTHEADING','Featured element')),
			TextField::create("FeaturedElementTitle", _t(__CLASS__ . '.FEATUREDELEMENTTITLE','Title')),
			TextField::create("FeaturedElementSubtitle", _t(__CLASS__ . '.FEATUREDELEMENTSUBTITLE','Subtitle')),
			$featuredContentField = HtmlEditorField::create('FeaturedElementContent', _t(__CLASS__ . '.FEATUREDELEMENTCONTENT','Featured element content'))
				->setDescription(_t(__CLASS__ . '.FEATUREDELEMENTCONTENTHELP','Write short and effective lead text about the featured items.')),
			HeaderField::create(_t(__CLASS__ . '.FEATUREDITEMS','Featured items'))
		]);

		// Additional tools to GridField
		$gridFieldConfig = GridFieldConfig_RelationEditor::create()->addComponents(
			new GridFieldToggleBoolean('Published'),
			new GridFieldAddExistingSearchButton(),
			new GridFieldDeleteAction()
		);	

		// Setup GridField Column
		$featuredTab = $fields->findOrMakeTab('Root.FeaturedItems', _t(__CLASS__ . '.TABFEATURED','Featured Items'));	
		$gridField = GridField::create("FeaturedItems", _t(__CLASS__ . '.FEATUREDITEMS', 'Featured Items:'), $this->owner->FeaturedItems()->sort('SortOrder'), $gridFieldConfig);
		$featuredTab->push($gridField);

		// Add the OrderableRows component if the parent object has an ID
		if($this->owner->ID) {
		    $gridField->getConfig()->addComponent(
		        new GridFieldOrderableRows('SortOrder')
		    );
		}

		$featuredContentField->addExtraClass('stacked')->setRows(3);

		return $fields;
	}
	
	// Featured items, which are published in current page
	// Published buttons
	function FeaturedItemsList($limit = false) {

		$items = $this->owner->FeaturedItems()->filter('Published', true)->sort('SortOrder');
		if($limit !== false) {
			$items = $items->limit($limit);
		}
		return $items;
	}
	
	// Count Featured Items, which are published, linked to current page and in current locale.
	// Used for automatic Bootstrap column amount counting, but limited in max 4 columns
	function countFeaturedItems() {

		return 12 / max(1, $this->owner->FeaturedItemsList(4)->filter('Published', true)->count());

	}
}